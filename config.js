Object.assign(process.env, {
  GIT_AUTHOR_NAME: 'Renovate Bot',
  GIT_AUTHOR_EMAIL: 'gitlab.renovatebot@gmail.com',
  GIT_COMMITTER_NAME: 'Renovate Bot',
  GIT_COMMITTER_EMAIL: 'gitlab.renovatebot@gmail.com',
});

module.exports = {
  endpoint: process.env.CI_API_V4_URL,
    executionTimeout: 2440, // minutes
  hostRules: [
    {
      matchHost: 'https://gitlab.com',
      username: 'Nomadic_Wall_bot',
      password: process.env.GITLAB_REGISTRY_TOKEN,
    },
    {
      matchHost: process.env.CI_API_V4_URL,
      hostType: 'pypi',
      token: process.env.RENOVATE_TOKEN,
    },
    {
      matchHost: process.env.CI_API_V4_URL,
      hostType: 'npm',
      token: process.env.RENOVATE_TOKEN,
    },
    {
      matchHost: process.env.CI_API_V4_URL,
      hostType: 'packagist',
      token: process.env.RENOVATE_TOKEN,
    },
  ],
  platform: 'gitlab',
  persistRepoData: true,
  username: 'Nomadic_Wall_bot',
  gitAuthor: 'Nomadic_Wall_bot <gitlab.renovatebot@gmail.com>',
  autodiscover: true,
};